.PHONY: dev-tools release-dry-run 
 
dev-tools:
	@npm install -g \
		@commitlint/cli@17 \
		@commitlint/config-conventional@17 \
		semantic-release@21 \
		@semantic-release/commit-analyzer@10 \
		@semantic-release/release-notes-generator@11 \
		@semantic-release/changelog@6 \
		@semantic-release/git@10 \
		conventional-changelog-conventionalcommits@6


# https://github.com/semantic-release/git#environment-variables
export GIT_AUTHOR_NAME="$(shell git config user.name)"
export GIT_AUTHOR_EMAIL=$(shell git config user.email)
export GIT_COMMITTER_NAME="$(shell git config user.name)"
export GIT_COMMITTER_EMAIL=$(shell git config user.email)

release-prep:
	@git checkout master
	@git pull origin master

release-dry-run: release-prep
	@npx semantic-release --no-ci --dry-run

release: release-prep
	@echo "This will generate and push a changelog update and a new tag, are you sure? [y/N] " && read ans && [ $${ans:-N} = y ]
	@npx semantic-release --no-ci

## 1.0.0 (2023-12-19)


### 🐛 Bug Fixes 🐛

* add npm install ([d0c7ec9](https://gitlab.com/renjithvr11/kaniko-tools/commit/d0c7ec9d141c5110557fc9c8d0278f4290b9858a))
* add npm install ([5a11579](https://gitlab.com/renjithvr11/kaniko-tools/commit/5a1157977504446f270aaf39e5fc94153d7c3a64))
